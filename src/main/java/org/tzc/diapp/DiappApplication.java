package org.tzc.diapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiappApplication {

	public static void main(String[] args) {
		System.setProperty("spring.config.name", "diapp");
		SpringApplication.run(DiappApplication.class, args);
	}
}
